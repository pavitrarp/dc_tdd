import pymongo


class MongoHandler:

    def __init__(self):
        self.mongo_client = pymongo.MongoClient("mongodb://localhost:27017/")

    def get_db(self, db):
        return self.mongo_client[db]

    def drop_db(self, db):
        db = self.get_db(db)
        return self.mongo_client.drop_database(self.mongo_client[db])

    def update(self, db, coll, find_json, update_json):
        db = self.get_db(db)
        return db[coll].update_one(find_json, update_json)

    def insert(self, db, coll, insert):
        db = self.get_db(db)
        return db[coll].insert_one(insert)

    def find_one(self, db, coll, find):
        db = self.get_db(db)
        return db[coll].find_one(find)

    def delete_one(self, db, coll, find_json, delete_json):
        db = self.get_db(db)
        return db[coll].delete_one(find_json, delete_json)