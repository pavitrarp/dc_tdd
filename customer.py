from mongo_handler import MongoHandler


class CustomerHandler(object):

    def create_customer(self):
        insert_data = MongoHandler().insert('tdd_db', 'tdd_collection', {'name': 'Pavitra RP'})

        return insert_data

    def update_customer(self):
        insert_data = MongoHandler().update('tdd_db', 'tdd_collection', {'name': 'Pavitra RP'},
                                            {"$set": {'name': 'Pavitra P'}})

        return insert_data
