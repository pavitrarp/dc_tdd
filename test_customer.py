import unittest
from customer import CustomerHandler


class Tdd(unittest.TestCase):

    def test_create_customer(self):
        customer_data = CustomerHandler()
        result = customer_data.create_customer()
        return result

    def test_update_customer(self):
        customer_data = CustomerHandler()
        result = customer_data.update_customer()
        return result


if __name__ == '__main__':
    unittest.main()
